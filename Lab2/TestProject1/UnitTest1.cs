namespace TestProject1;
using Main;
public class Tests
{
    private ModelTests tests = new ModelTests();
    [Test]
    public void Test1()
    {
        Assert.AreEqual(tests.Sweets.Select(sweet => sweet.CaloriesCount), new List<int>(
        {
            130,
            75,
            210,
            160,
            100,
        });
    }
}