﻿namespace Main;

public class Sweet
{
    public Sweet(string name, int caloriesCount, int categoryID)
    {
        Name = name;
        CaloriesCount = caloriesCount;
        CategoryID = categoryID;
    }

    public override string ToString()
    {
        return $"This is a {Name}, the category {CategoryID.ToString()}. It has {CaloriesCount.ToString()} calories.";
    } 
    public string Name { get; set; }
    public int CaloriesCount { get; set; } 
    public int CategoryID { get; set; }
    
}

public class SweetCategory
{
    public string Name { get; set; }
    public  int ID { get; set; }
}

public class ModelTests
{
    public List<SweetCategory> Categories
    {
        get
        {
            return new List<SweetCategory>()
            {
                new SweetCategory() { Name = "Boiled", ID = 001 },
                new SweetCategory() { Name = "Chocolate", ID = 002 },
                new SweetCategory() { Name = "Caramel", ID = 003 },
                new SweetCategory() { Name = "Chewy", ID = 004 },
            };
        }
    }

    public List<Sweet> Sweets
    {
        get
        {
            return new List<Sweet>()
            {
                new Sweet("Gummy Bears", 130, 004),
                new Sweet("Lollipop", 75, 001),
                new Sweet("Kit Kat", 210, 002),
                new Sweet("Toffee", 160, 003),
                new Sweet("Jelly Beans", 100, 004),
            };
        }
    }
}