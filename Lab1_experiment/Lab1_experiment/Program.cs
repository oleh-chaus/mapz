﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
namespace Lab1_experiment
{
    class Program
    {
        class TestClass
        { 
            public void TestMethod() { }
        }

        static void TestMethodCallDefault()
        {
            var testClass = new TestClass();
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for (int i = 0; i < 10_000_000; i++)
            {
                testClass.TestMethod();
            }
            Console.WriteLine($"Time taken by TestMethodCallDefault: {stopWatch.ElapsedMilliseconds}ms");
        }
        static void TestMethodCallReflection()
        {
            var testClass = new TestClass();
            Stopwatch stopWatch = new Stopwatch();
            MethodInfo myMethod = testClass.GetType().GetMethod("TestMethod");
            stopWatch.Start();
            for (int i = 0; i < 10_000_000; i++)
            {
                myMethod.Invoke(testClass, null);
            }
            Console.WriteLine($"Time taken by TestMethodCallReflection: {stopWatch.ElapsedMilliseconds}ms");
        }

        static void TestClassCreationDefault()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for (int i = 0; i < 10_000_000; i++)
            {
                var myObject = new TestClass();
            }

            stopWatch.Stop();
            Console.WriteLine($"Time taken by TestClassCreationDefault: {stopWatch.ElapsedMilliseconds}ms");
        }

        static void TestClassCreationReflection()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            for (int i = 0; i < 10_000_000; i++)
            {
                Type classType = typeof(TestClass);
                var myObject = (TestClass)Activator.CreateInstance(classType);
                MethodInfo myMethod = classType.GetMethod("TestMethod");
                myMethod.Invoke(myObject, null);
            }

            stopWatch.Stop();
            Console.WriteLine($"Time taken by TestClassCreationReflection: {stopWatch.ElapsedMilliseconds}ms");
        }
        static void Main(string[] args)
        {
            //TestDefault();
            //TestReflection();
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"Experiment {i}:");
                TestClassCreationDefault();
                TestClassCreationReflection();
            }
            Console.ReadLine();
        }
    }
}
