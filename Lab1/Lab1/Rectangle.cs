﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public abstract class Rectangle
    {
        protected double Width;
        protected double Height;

        public Rectangle(double width, double height)
        {
            Width = width;
            Height = height;
        }

        public virtual string GetName()
        {
            return "I am a rectangle";
        }
    }
}
