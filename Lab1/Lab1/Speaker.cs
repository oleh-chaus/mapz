﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    struct Speaker
    {
        string Color;
        int Loudness;
        Speaker(string color, int loudness)
        {
            Color = color;
            Loudness = loudness;
        }
        
        void Play()
        {
            Console.WriteLine($"*{Color} speaker noises ({Loudness} dB)*");
        }
    }
}
