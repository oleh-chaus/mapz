﻿using System;

namespace Lab1
{
    class Program
    {
        static void refChangeString(ref string par)
        {
            par = "Hello";
        }

        static void outChangeString(out string par)
        {
            par = "Goodbye";
        }

        static void Main(string[] args)
        {
            var newCar = new Car();
            newCar.Make = "asddasd";
            Console.WriteLine(newCar.Equals( new Car()));
            Console.WriteLine(newCar.ToString());
            //newCar.Year = 0;

            //var myCar = new Car("Obey", "9F", 2013, "white");
            //Console.WriteLine(myCar);       // implicit conversion
            //Console.WriteLine((bool)myCar); //explicit conversion (cast)

            //float floatVal = 123.1412F;
            //double converted = floatVal; 

            //int i = 10;
            //object o = i; //boxing
            //i = 100;
            //i = (int)o; //unboxing
            //Console.WriteLine(i);

            //string testString = "ababab";
            //Console.WriteLine(testString);

            //refChangeString(ref testString);
            //Console.WriteLine(testString);

            //outChangeString(out testString);
            //Console.WriteLine(testString);
            //var BaseClass = new AccessModifiers();
            //var DerivedClass = new Derived();

            //BaseClass.Test();
            //DerivedClass.DerivedTest();

            //var outer = new Outer();
            //outer.Greet();

            //var duck = new Duck();
            //duck.Fly();
            //duck.Walk();
            //duck.Swim();

            //Car myCar1 = new Car("Honda", "Civic", 2020, "red");
            //Car myCar2 = new Car("Toyota", "Camry", 2022);

            //myCar1.Drive();
            //myCar2.Drive(15);

            //var TestClass = new DefaultAccessModifiers();
            //TestClass.SayHello();
            //Console.WriteLine(TestClass.time);
            //var TestClassNested = new DefaultAccessModifiers.NestedClass();

            //var speaker = new Speaker();
            //Console.WriteLine(speaker.Color);
            //Console.WriteLine(speaker.Loudness);
            //speaker.Play();

            //var square = new Square(1);
            //Console.WriteLine(IceCreamFlavor.Chocolate & IceCreamFlavor.Vanilla); // 0 & 1 = 0
            //Console.WriteLine(IceCreamFlavor.CookieDough | IceCreamFlavor.Mint); // 100 | 011 = 111 (7)
            //Console.WriteLine(IceCreamFlavor.Strawberry ^ IceCreamFlavor.RockyRoad); // 010 ^ 101 = 111 (7)


            //IceCreamFlavor MyFavoriteFlavor = IceCreamFlavor.Chocolate;
            //Console.WriteLine(MyFavoriteFlavor == IceCreamFlavor.Chocolate && MyFavoriteFlavor == IceCreamFlavor.Vanilla);
            //Console.WriteLine(MyFavoriteFlavor == IceCreamFlavor.CookieDough || MyFavoriteFlavor == IceCreamFlavor.Mint);

            //var car1 = new Car("", "", 0, false);
            //var car2 = new Car("", "", 0, false);
        }
    }
}
