﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class Duck : IFlying, IWalking, ISwimming
    {
        public void Fly()
        {
            Console.WriteLine("The duck is flying.");
        }
        public void Walk()
        {
            Console.WriteLine("The duck is walking.");
        }
        public void Swim()
        {
            Console.WriteLine("The duck is swimming.");
        }
    }

}
