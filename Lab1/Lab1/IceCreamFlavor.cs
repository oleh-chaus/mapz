﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public enum IceCreamFlavor
    {
        Vanilla,
        Chocolate,
        Strawberry,
        Mint,
        CookieDough,
        RockyRoad,
    }
}
