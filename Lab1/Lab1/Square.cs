﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class Square : Rectangle, IHasArea
    {
        internal bool isCool = true;
        public Square(double length) : base(length, length)
        { }
        public double Area()
        {
            return Width * Height;
        }
        public override string GetName()
        {
            return "I am a Square";
        }
    }
}
