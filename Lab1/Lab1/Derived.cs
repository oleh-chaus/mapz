﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class Derived : AccessModifiers
    {
        public void DerivedTest()
        {
            Console.WriteLine(PublicVar);
            Console.WriteLine(ProtectedVar);
        }
    }
}
