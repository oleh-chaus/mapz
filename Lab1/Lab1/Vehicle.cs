﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public abstract class Vehicle
    {
        internal string Make { get; set; }
        public string Model { get; set; }
        protected int Year { get; set; }
        public bool IsElectric { get; set; }
        private void Explode()
        {
            Console.WriteLine("It is quite unfortunate to inform you that I have exploded.");
        }

        public Vehicle()
        {
            Make = "";
            Model = "";
            Year = 0;
            IsElectric = false;
        }
        public Vehicle(string make, string model, int year)
        {
            Make = make;
            Model = model;
            Year = year;
            IsElectric = false;
        }
    }
}
