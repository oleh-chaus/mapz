﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class Car : Vehicle, IDrivable
    {
        public string Color { get; set; }
        public static List<Car> cars;
        static Car()
        {
            cars = new List<Car>();
        }
        public Car(string make, string model, int year, string color) : base(make, model, year) //`base` used to access base constructor
        {
            cars.Add(this);
            this.Color = color; //`this` used to access the object that called the constructor
        }
        public Car(string make, string model, int year, bool isElectric) : this(make, model, year, "white") // `this` used to call another constr.
        {
            base.IsElectric = isElectric; //`base` used to access base class property
        }

        public Car() : base() { Color = ""; }

        public void Drive()
        {
            Console.WriteLine($"Driving my {Color} {Year} {Make} {Model}");
        }

        public void Drive(int miles)
        {
            Console.WriteLine($"Driving my {Color} {Year} {Make} {Model} for {miles} miles");
        }

        public static void SayHello()
        {
            Console.WriteLine("Hello!");
        }

        public override bool Equals(object obj)
        {
            return obj is Car car &&
                   car.Color == Color;
        }

        public override string ToString()
        {
            return $"{Color} {Year} {Make} {Model}";
        }

        public static implicit operator string(Car c) => $"{c.Color} {c.Year} {c.Make} {c.Model}";
        public static explicit operator bool(Car c) => c.IsElectric;
    }
}
