﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    public class AccessModifiers
    {
        public int PublicVar = 1;
        private int PrivateVar = 2;
        protected int ProtectedVar = 3;

        public void Test()
        {
            Console.WriteLine(PublicVar);
            Console.WriteLine(PrivateVar);
            Console.WriteLine(ProtectedVar);
        }
    }
}
